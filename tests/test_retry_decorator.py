#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import unittest
from context import IDE_utils as retry
from com.sun.star.connection import NoConnectException

retry.CONNECT_REPORT = lambda *args: None  # Silent connections
#retry.CONNECT_REPORT = print  # Verbose connection(s)

class Test_Retry_Decorator(unittest.TestCase):
    
    def test_No_Socket_NoConnectException(self):
        # faulty socket connection & no retry
        retry.CONNECT_DELAYS= (0,)
        self.assertRaises(NoConnectException, retry.connect)
        
    def test_No_Pipe_NoConnectException(self):
        # missing pipe & 2 retries
        retry.CONNECT_DELAYS= (0, 1.75)  # (1.9, 2.8, 3.7, 4.6)
        self.assertRaises(NoConnectException,
            retry.connect, pipe='freeOffice')
    # if pipe='€' nothing is executed !!
    
    def test_Verbose_NoConnectException(self):
        #retry.CONNECT_REPORT = print  # Verbose test(s)
        self.assertRaises(NoConnectException, retry.connect) 

if __name__ == '__main__':

    unittest.main()

