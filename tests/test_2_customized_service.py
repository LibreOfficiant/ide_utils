#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import logging, os, sys
from context import IDE_utils as __, getConfiguration as config

__.CONNECT_REPORT = print  # Stdout retried connections

def _getService():
    # soffice script used on *ix, Mac; soffice.exe used on Win
    # --startup LibreOffice options; -startup OpenOffice options  
    if "UNO_PATH" in os.environ:
        sOffice = os.environ["UNO_PATH"]
    else:
        sOffice = "" # hope for the best

    sOffice = os.path.join(sOffice, "soffice")
    if sys.platform.startswith("win"):
        sOffice += ".exe"
    #print(sOffice)

    options = ['"-accept=pipe,name=LinusTorvalds;urp;"', '-invisible']
    if sys.version_info.major == 3:  # OpenOffice uses 2.x
        options = ['"--accept=pipe,name=LinusTorvalds;urp;"', '--invisible']
    #print(options)

    return sOffice, options

OFFICE, OPTIONS = _getService()
pgm = {OFFICE: OPTIONS}

class Test_Customized_Service(unittest.TestCase):
    
    def test_Example_2(self):
        with __.Runner(soffice=pgm) as jesse_owens:  # Start/Stop
            XSCRIPTCONTEXT = __.XSCRIPTCONTEXT  # Connect/Adapt
            self.assertTrue(XSCRIPTCONTEXT)
            #self.assertTrue(jesse_owens.pool)
            
    def tst_Example_2_Badly_Coded(self):
        with __.Runner(soffice=pgm) as carl_lewis:  # Start/Stop
            pass  # __.XSCRIPTCONTEXT gets overwritten here
        self.assertRaises( TypeError,
            self.assertTrue( __.XSCRIPTCONTEXT ) )
        #XSCRIPTCONTEXT = __.XSCRIPTCONTEXT  # Connect/Adapt
        #self.assertTrue(XSCRIPTCONTEXT)  # officehelper pipe is set
        
    def test_Nbr_of_Contexts_Not_Null(self):
        self.assertTrue(__.ScriptContext.pool) 

if __name__ == '__main__':

    unittest.main()

