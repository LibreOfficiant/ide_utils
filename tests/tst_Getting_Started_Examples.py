#!/usr/bin/python
# -*- coding: utf-8 -*-

import uno

def macro_1(): pass  # Your code goes here
def macro_2(): pass  # Your code goes here
def macro_3(): pass  # Your code goes here
my_own_macro = macro_1
def a_macro(): print('Chester Gould was here')

g_exportedScripts = macro_1, macro_2, macro_3  # *Office public macros


import logging  # Trace execution
logging.getLogger().setLevel(logging.DEBUG)

#! EXAMPLE 1
if __name__ == " __main__":
    """ IDE runnable code: *Office as a service """
    import IDE_utils as geany
    with geany.Runner() as jesse_owens:  # Start/Stop
        XSCRIPTCONTEXT = geany.XSCRIPTCONTEXT  # Connect/Adapt 
        macro_1 # Run

import platform
if platform.uname().release == '8':
    LIBRE_OFFICE = 'C:\\Program Files\\LibreOffice 5\\program\\soffice.exe'
else:
    LIBRE_OFFICE = 'D:\\Program Files\\LibreOffice 5\\program\\soffice.exe'
FOREGROUND = ['--accept="pipe,name=LibreOffice;urp;"'
              ]  # LibreOffice - Foreground - visible instance
BACKGROUND = ['--accept="socket,host=localhost,port=2002;urp;"',
              '--headless',
              #'--invisible',
              '--minimized',
              '--nodefault',
              '--nologo',
              '--norestore',
              #'--safe-mode',
              #'--unaccept={UNO-URL}',
              #'--unaccept=all',
              '--language=fr'
              ]  # LibreOffice - Background - localhost,port#

#! EXAMPLE 2
if __name__ == " __main__":
    pgm = {LIBRE_OFFICE:
        ['--accept=pipe,name=LinusTorvalds;urp;', '--invisible']}
    import IDE_utils as ide  # Start
    with ide.Runner(soffice=pgm) as carl_lewis:  # Start/Stop
        XSCRIPTCONTEXT = ide.XSCRIPTCONTEXT  # Adapt
        macro_1()  # Run#! EXAMPLE 2

#! EXAMPLE 3 that is EXAMPLE 1
if __name__ == " __main__":
    import IDE_utils as office  # Start
    with office.Runner() as usain_bolt:  # Start/Stop
        XSCRIPTCONTEXT = office.XSCRIPTCONTEXT  # Adapt
        macro_2()  # Run

#! EXAMPLE 4
if __name__ == " __main__":
    import IDE_utils as libO5  # Start
    ctx = libO5.connect(pipe='LinusTorvalds')
    XSCRIPTCONTEXT = libO5.ScriptContext(ctx)  # Adapt
    macro_2()  # Run

#! EXAMPLE 5
if __name__ == " __main__":
    import IDE_utils as aOO4  # Start
    ctx = aOO4.connect(host='localhost', port=2017, flush=True)
    XSCRIPTCONTEXT = aOO4.ScriptContext(ctx)  # Adapt
    macro_2()  # Run

#! EXAMPLE 6
if __name__ == " __main__":
    from IDE_utils import RUNNERS, Runner, \
        XSCRIPTCONTEXT  # Connect, Adapt
    RUNNERS = 'myFile.json'
    with Runner() as jason_argonaut:  # Start, Stop
        my_own_macro()  # Run

#! EXAMPLE 7
if __name__ == "__main__":
    import logging
    logging.getLogger().setLevel(logging.DEBUG)

    from IDE_utils import Runner, XSCRIPTCONTEXT  # Connect, Adapt
    pgm = {'soffice.€xe': ['--accept', '--headless']}
    with Runner(soffice=pgm) as dick_tracy:  # Start, Stop
        a_macro()  # Run









#! EXAMPLE y
if __name__ == " __main__":
    import IDE_utils as usain_bolt  # Runner()
    usain_bolt.RUNNERS = 'XSCRIPTCONTEXT.json'
    try:
        usain_bolt.start()
        ctx = usain_bolt.connect()
        XSCRIPTCONTEXT = usain_bolt.ScriptContext(ctx)  # Adapt
        pass  # Your code goes here
    finally:
        usain_bolt.stop()

#! EXAMPLE y.b
if __name__ == " __main__":
    import IDE_utils as ide
    ide.DEBUG = True
    usain_bolt = ide.Runner()  # optional default JSON config
    try:
        instances = {LIBRE_OFFICE: FOREGROUND}
        ide.start(soffice=instances)  # starts extra 'soffice' instances
        ctx = ide.connect(pipe='LibreOffice')
        XSCRIPTCONTEXT = ide.ScriptContext(ctx)  # Adapt
        pass  # Your code goes here
    finally:
        ide.stop()


