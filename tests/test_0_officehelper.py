#!/usr/bin/python
# -*- coding: utf-8 -*-

from unittest import TestCase, main
from context import IDE_utils as oh, getConfiguration as Config

class Test_OfficeHelper(TestCase):

    def test_SCRIPTCONTEXT_Exists(self):
        self.assertTrue(oh.XSCRIPTCONTEXT)
        Config(oh.XSCRIPTCONTEXT)

    def test_Context_isSet(self):
        self.assertTrue(oh._ctx)
        Config(oh.XSCRIPTCONTEXT)

    def test_XSCRIPTCONTEXT_isPooled(self):
        self.assertTrue('officehelper' in oh.ScriptContext.pool)
        Config(oh.XSCRIPTCONTEXT)

if __name__ == '__main__':

    main()

