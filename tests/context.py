""" Initialize running context for Unit Testing """

import logging
import os
import sys
import uno

''' Add my package or module to sys.path for testing '''
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import IDE_utils  # OUR module or package
# Credits: 
#   " the Hitchhiker's Guide to Python " by Kenneth Reitz, Apr. 2018

IDE_utils._INFO, IDE_utils._DEBUG = False, False
''' Unit tests execution options '''
# IDE_utils._EMULATE_OFFICEHELPER = True  # by default

def getConfiguration(scriptContext):  # as namedtuple
    ''' Sample macro '''
    from com.sun.star.beans import PropertyValue
    prop = PropertyValue()
    prop.Name = 'nodepath'
    prop.Value = '/org.openoffice.Setup/Product'
    properties = (prop,)
    
    ctx = scriptContext.getComponentContext()
    configuration = ctx.ServiceManager.createInstanceWithContext(
        'com.sun.star.configuration.ConfigurationProvider', ctx)
    info = configuration.createInstanceWithArguments( 
        'com.sun.star.configuration.ConfigurationAccess', properties)
    name = info.getByName('ooName')
    vendor = info.getByName('ooVendor')
    setupVersion = info.getByName('ooSetupVersion')
    setupVersionAboutBox = info.getByName('ooSetupVersionAboutBox')
    # xmlFileFormatVersion = info.getByName('ooXMLFileFormatVersion')  # Missing in 6.0.4
    
    from collections import namedtuple as nt
    Configuration = nt('Configuration', ['name', 'vendor', 'setupVersion'])
    cfg = Configuration(name, vendor, setupVersion)
    logging.info(cfg)
    
    return cfg

# Module structure:

# import
# constants
# exception classes
# interface functions
# classes
# internal functions & classes

# if __name__ == '__main__':
#     status = main()
#     sys.exit(status)

# Credit: Code Like a Pythonista, by David Goodger, Circa. 2008
