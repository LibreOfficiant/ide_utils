# ~ from __future__ import print_function # for OpenOffice
import uno #, inputBox

def macro_1(): pass
def macro_2(): pass  # Your code goes here

g_exportedScripts = macro_1, macro_2  # *Office public macros


if __name__ == " __main__":
    ''' IDE runnable code: *Office as a service '''
    import IDE_utils as office
    with office.Runner() as jesse_owens:  # Start/Stop
        XSCRIPTCONTEXT = office.XSCRIPTCONTEXT  # Connect/Adapt
        macro_2()  # Run

if __name__ == " __main__":
    ''' Connections using ScriptContext class '''

    import IDE_utils as geany
    ctx = geany.connect(pipe='LinusTorvalds')
    # ~ ctx = geany.connect(uno_url='uno:pipe,name=LinusTorvalds;urp;StarOffice.ServiceManager')
    XSCRIPTCONTEXT = geany.ScriptContext(ctx)  # Adapt

    import IDE_utils as pycharm
    # ~ RETRY_DELAYS = (0, 5, 10, 15)
    # ~ RETRY_REPORT = print
    # ~ ctx = pycharm.connect(uno_url="uno:socket,host=127.0.0.1,port=2017;urp;StarOffice.ServiceManager")
    ctx = pycharm.connect(host='localhost',port=2017)
    XSCRIPTCONTEXT = pycharm.ScriptContext(ctx)  # Adapt

if __name__ == "__main__":
    ''' Combined Start, stop using Runner class '''

    from sys import platform
    if platform.startswith('linux') or platform == 'darwin':
        pgm = {"soffice": ['-accept="pipe,name=LinusTorvalds;urp;"',
                           '-headless', '-nologo']}
        #~ pgm = {"/opt/openoffice4/program/soffice":
               #~ ['-accept="pipe,name=LinusTorvalds;urp;"']}
        #~ pgm = {"/usr/lib/libreoffice/program/soffice":
               #~ ['--accept="pipe,name=LinusTorvalds;urp;"']}
    elif platform.startswith('win'):
        #~ pgm = {"D:\PortableApps\OO.o-3.2\App\openoffice\program\soffice.exe":
               #~ ['-accept="pipe,name=LinusTorvalds;urp;"']}
        #~ pgm = {"D:\PortableApps\aOO-4.1.5\App\openoffice\program\soffice.exe":
               #~ ['-accept="pipe,name=LinusTorvalds;urp;"']}
        pgm = {"C:\Program Files\LibreOffice 5\program\soffice.exe":
               ['--accept="pipe,name=LinusTorvalds;urp;"', '--invisible']}
    else:
        raise RuntimeError(' Unsupported {} platform'.format(platform))
    print(platform, pgm)
    import IDE_utils as ide
    with ide.Runner(soffice=pgm) as carl_lewis:  # Start, Stop
        print(carl_lewis)
        XSCRIPTCONTEXT = ide.XSCRIPTCONTEXT  # Connect, Adapt
        #~ i.e. XSCRIPTCONTEXT = ide.ScriptContext(ide.connect(pipe='LinusTorvalds'))
        macro_2  # Run

    with IDE_utils.Runner() as usain_bolt:  # Start, Stop
        print(usain_bolt)
        XSCRIPTCONTEXT = IDE_utils.XSCRIPTCONTEXT  # Connect, Adapt
        macro_1  # Run

""" TERMINATE """
if __name__ == " __main__":
    import IDE_utils as ide
    from sys import platform
    if platform.startswith('linux') or platform == 'darwin':
        ''' Exception thrown on looped terminate() with OO.o 3.2

        uno.RuntimeException
        '''
        XSCRIPTCONTEXT.getDesktop().terminate()
        ''' one call is enough for OO.o 3.2 multiple instances '''
    elif platform.startswith('win'):
        ide.stop()
    else:
        raise RuntimeError(' Unsupported {} platform'.format(platform))


