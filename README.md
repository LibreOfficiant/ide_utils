# IDE_utils - Using Python macros in IDEs<br><br>for LibreOffice or OpenOffice

### Intent
Developing Python macros within an Integrated Development Environment ([IDE](https://en.wikipedia.org/wiki/Integrated_development_environment)) is possible as indicated in [Designing & Developing Python Applications](https://wiki.documentfoundation.org/Macros/Python_Design_Guide).

IDE_utils module simplifies Python macros development for LibreOffice or OpenOffice. Non-Basic developpers know that *Office provides XSCRIPTCONTEXT built-in as an genuine entry point to its API. Using a Python Integrated Development Environment ([IDE](https://en.wikipedia.org/wiki/Integrated_development_environment)) outside of *Office means initializing a Python bridge, which adds supplemental code in order to reach product API.

A (Libre/Open)Office Python macro looks like:
```python
import uno
def my_1st_macro():
    # Won't run directly in Anaconda, Geany, KDevelop, PyCharm or else
    doc = XSCRIPTCONTEXT.getDocument()
    doc.getText().setString("Hello World!")

g_exportedScripts = my_1st_macro,
```
Direct editing and testing of Python macros inside LibreOffice requires solely APSO[^apso], in which case a Python IDE is not needed.

Executing macros, in your preferred IDE, requires to bridge together the IDE and (Libre/Open)Office. Once done UNO objects become accessible, and you can start benefitting from the bells and whistles of IDEs such as Geany, Pyzo or PyCharm to name a few.

Up to five steps can be necessary to achieve such execution:

1. **Start** LibreOffice as a service,
2. **Connect** to a service,
3. create a XSCRIPTCONTEXT **Adapt**or,
4. **Run** the macro,
5. **Stop** LibreOffice as a service

These steps imply, from few to many Python extra code, to be removed from your macro, once ready for validation inside (Libre/Open)Office.

### Module Features
IDE_utils module *innocuously integrates in Python* macros with the following features:
* start, connect, adapt, run and stop steps are optional
* Support multiple platforms i.e. essentially Linux, MacOs and Windows
* on-demand startup --options
* Permit pipe or socket connections
* decoupled coding using [Inversion of Control](https://en.wikipedia.org/wiki/Inversion_of_control)
* Provide service pooling, context pooling
* [KISS](https://en.wikipedia.org/wiki/KISS_principle)

**IDE_utils' content:** A Runner() [context manager](https://docs.python.org/3/library/contextlib.html?highlight=context%20manager#) class is responsible for starting and stopping *soffice* instances. A connect() function bridges the actual IDE and LibreOffice instances. A ScriptContext() object is injected as XSCRIPTCONTEXT built-in.

### Usage

With [IDE_utils.py](https://gitlab.com/LibreOfficiant/ide_utils) a LibreOffice Python macro library may ressemble this example:

```python
import uno
def my_macro(): pass  # Your code goes here

g_exportedScripts = my_macro,

if __name__ == "__main__":
    ''' IDE runnable code: *Office as a service '''
    from IDE_utils import Runner, XSCRIPTCONTEXT
    with Runner() as jesse_owens:  # Start/Stop, Connect/Adapt
        my_macro()  # Run
```
**Example:** *jesse_owens* establishes a default bridge[^oh], a valid context in injected as XSCRIPTCONTEXT, my_macro gets executed then *jesse_owens* removes the bridge.

officehelper[^oh] bootstrap() function documentation details what happens here : *The soffice process is started opening a named pipe of random name, then the local context is used to access the pipe. This function directly returns the remote component context, from whereon you can get the ServiceManager by calling getServiceManager() on the returned object.*

**Start**ing and **stop**ping a service may not fit all situations, thus those steps are optional. Customizing your service running conditions is also possible by overriding officehelper[^oh] default service. Three different ways to use IDE_utils are documented [here](https://gitlab.com/LibreOfficiant/ide_utils/tree/master/docs).

### Installation
0. Ensure your IDE points to (Libre/Open)Office embedded Python as described in [Python Basics](https://wiki.documentfoundation.org/Macros/Python_Basics)
1. Copy IDE_utils module into your <OFFICE>/program/ directory OR Include it into your IDE project directory
2. Include one of the examples into your Python macro
3. Run your (Libre/Open) macro from your preferred IDE

### Presentations and demos
[Python Scripts made simple w/ IDE_utils](https://www.libreoffice.org/assets/libocon2020/Slides/oSLO-2020.IDE-utils.pdf) PDF presentation [including videos](https://gitlab.com/LibreOfficiant/ide_utils/-/raw/master/docs/OsLo_2020.IDE_utils.7z) - Oct. 2020

[Scripting LibO Python Macros](https://libocon.org/assets/Conference/Almeria/Alain-Python.4.final.pdf) PDF presentation - Almeria Sep. 2019


### Notes

[^apso]: [Alternative Python Script Organizer](https://gitlab.com/jmzambon/apso) extension
[^oh]: officehelper.py is a LibreOffice and OpenOffice standard module
