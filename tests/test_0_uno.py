#!/usr/bin/python
# -*- coding: utf-8 -*-

from unittest import TestCase, main

class Test_Uno(TestCase):
    ''' Universal Network Objects have to be reachable '''

    def test_uno_isImported(self):
        import uno
        self.assertTrue(uno)

    def test_Program_Path_getter(self):
        import os
        self.assertTrue('UNO_PATH' in os.environ)


if __name__ == '__main__':

    main()
