#!/usr/bin/python
# -*- coding: utf-8 -*-

from unittest import TestCase, main
from context import IDE_utils as geany, getConfiguration as config

class Test_Recommended_Use(TestCase):
    
    def test_Example_1(self):
        with geany.Runner() as jesse_owens:  # Start/Stop
            XSCRIPTCONTEXT = geany.XSCRIPTCONTEXT  # Connect/Adapt
            self.assertTrue(XSCRIPTCONTEXT)
            config(XSCRIPTCONTEXT)
            
    def test_Example_1_Badly_Coded(self):
        with geany.Runner() as carl_lewis:  # Start/Stop
            pass
        XSCRIPTCONTEXT = geany.XSCRIPTCONTEXT  # Connect/Adapt
        self.assertTrue(XSCRIPTCONTEXT)  # officehelper pipe is set
        config(XSCRIPTCONTEXT)

    def test_Nbr_of_Contexts_Not_Null(self):
        self.assertTrue( len(geany.ScriptContext.pool)>0 )
        config(geany.XSCRIPTCONTEXT) 


if __name__ == '__main__':

    main()

