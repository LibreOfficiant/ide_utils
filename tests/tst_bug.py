#!/usr/bin/python
# -*- coding: utf-8 -*-
from unittest import TestCase, main
from context import IDE_utils as __  # 

__.EMULATE_OFFICEHELPER = True

class Test_Bug116156(TestCase):
    def test_SCRIPTCONTEXT_Exists(self):
        self.assertTrue(oh.XSCRIPTCONTEXT)
    def test_Context_isSet(self):
        self.assertTrue(oh._ctx)
    def test_XSCRIPTCONTEXT_isPooled(self):
        self.assertTrue('officehelper' in oh.ScriptContext.pool)

if __name__ == '__main__':

    main()
