#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest 
import json, logging, os, sys
from context import IDE_utils as __, getConfiguration as Config

__.CONNECT_REPORT = print
__._INFO, __._DEBUG = False, False


def _getService():
    # soffice script used on *ix, Mac; soffice.exe used on Win
    # --startup LibreOffice options; -startup OpenOffice options  
    if "UNO_PATH" in os.environ:
        sOffice = os.environ["UNO_PATH"]
    else:
        sOffice = "" # hope for the best

    sOffice = os.path.join(sOffice, "soffice")
    if sys.platform.startswith("win"):
        sOffice += ".exe"
    #print(sOffice)

    options = ['"-accept=socket,host=localhost,port=2017;urp;"',
        '-headless', '-minimized', '-nodefault', '-nologo']
    if sys.version_info.major == 3:  # OpenOffice uses 2.x
        options = ['"--accept=socket,host=localhost,port=2017;urp;"',
            '--headless', '--minimized', '--nodefault', '--nologo']
    #print(options)

    return sOffice, options

OFFICE, OPTIONS = _getService()
service = {OFFICE: OPTIONS}

class Test_Included_Service(unittest.TestCase):

    def setUp(self):
        # Create the external service file
        with open(__.RUNNERS, 'w') as f:
            json.dump(service, f)

    def tearDown(self):
        # Get rid of the external service file 
        if os.path.exists(__.RUNNERS):
            os.remove(__.RUNNERS)

    def tst_Example_3(self):
        
        with __.Runner() as usain_bolt:  # Start/Stop
            self.assertTrue(__.XSCRIPTCONTEXT)  # Connect/Adapt
            #self.assertTrue( len(jesse_owens.pool) == 1 )
            print(Config(__.XSCRIPTCONTEXT))
        #__.stop()

    def tst_Example_3_Badly_Coded(self):
        with __.Runner() as usain_bolt:  # Start/Stop
            pass  # __.XSCRIPTCONTEXT gets overwritten here
        self.assertRaises( TypeError,
            self.assertTrue( __.XSCRIPTCONTEXT ) )
        #XSCRIPTCONTEXT = __.XSCRIPTCONTEXT  # Connect/Adapt
        #self.assertTrue(XSCRIPTCONTEXT)  # officehelper pipe is set

    def test_Nbr_of_Contexts_Not_Null(self):
        self.assertTrue( __.ScriptContext.pool )
        

if __name__ == '__main__':

    unittest.main()

