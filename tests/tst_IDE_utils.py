#!/usr/bin/python
# -*- coding: utf-8 -*-
''' From primitive to complex Use Cases

    Elementary cases go first, then we elaborate upon them. Test cases
    are cascading from simple to convoluted.

'''

#~ WINDOWS = "Windows\"
#~ UNIX = 'Linux/'
#~ platform = WINDOWS

class ExceptionContextManager:
    # cf. https://tuvistavie.com/2017/unittest-assert-raises/
    def __init__(self, expected_exc, test_case):
        self.expected_exc = expected_exc
        self.test_case = test_case

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is None:
            self.test_case.fail("{0} was not raised".format(self.expected_exc))
        elif isinstance(exc_value, self.expected_exc):
            return True
        return False


import IDE_utils as geany, \
    logging, \
    officehelper, \
    time, \
    unittest
logging.getLogger().setLevel(logging.INFO)

from com.sun.star.connection import NoConnectException

logging.info('0. // tearDOWN pre-requisite test')
class Test_stop(unittest.TestCase):
    ''' killall `soffice` tasks '''
    def test_stop(self): pass
        # ~ geany.stop()


logging.info('1. officehelper // START, CONNECT')
class Test_officehelper(unittest.TestCase):
    ''' Native *Office bootstrap mechanism '''
    ''' Too many instances or memory full not tested '''
    def test_ctx(self):
        ctx = officehelper.bootstrap()
        keys = geany.ScriptContext.pool.keys()
        self.assertTrue( 'officehelper' in keys )
        self.assertTrue(ctx)


logging.info('2. ScriptContext() // ADAPT')
class Test_ScriptContext(unittest.TestCase):
    ''' Native LibreOffice bootstrap mechanism '''
    def setUp(self):
        ctx = officehelper.bootstrap()
        self.sc = geany.ScriptContext(ctx)

    def test_XSCRIPTCONTEXT(self):
        self.assertTrue(geany.XSCRIPTCONTEXT)
    def test_ScriptContext(self):
        self.assertTrue(self.sc)
    def test_getComponentContext(self):
        self.assertTrue(self.sc.getComponentContext())
    def test_getDesktop(self):
        self.assertTrue(self.sc.getDesktop())
    #~ def test_getDocument(self):
        #~ ''' NO_Document present by default '''
        #~ self.assertFalse(self.sc.getDocument())
        # Throws error when *Office desktop is started

logging.info('3. Runner() // START')
class Test_Start(): #unittest.TestCase):

    def test_StartPipe(self):
        geany.start(soffice='Start.LibrfrOffice.Pipe.json')
        geany.connect(pipe='LibreOffice')
        keys = geany.ScriptContext.pool.keys()
        self.assertTrue( 'LibreOffice' in keys )
        ctx = geany.ScriptContext.pool['LibreOffice']
        geany.ScriptContext(ctx).getDesktop().terminate()


logging.info('3. connect() // CONNECT')
class _connect():
    ''' NO started socket/pipe services (by default) '''
    def getExceptionContextManager(self, exc_type):
        return ExceptionContextManager(exc_type, self)

    # ~ def test_NoSocket(self):
        # ~ ctx = geany
        # ~ with self.getExceptionContextManager(NoConnectException) as ecm:
            # ~ geany.connect()
        # ~ self.assertTrue(ecm.exception is NoConnectException)
    # ~ def test_NoPipe(self):
        # ~ geany.connect(pipe='anyName')

class Fixture(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._pipe = geany.start()
        cls._socket = ...
    @classmethod
    def tearDownClass(cls):
        cls.destroyPipe(cls._pipe)
        cls.destroySocket(cls._socket)

# ~ import IDE_utils as ide
# ~ class  st_Runner(unittest.TestCase, ide.Runner):
    # ~ def setUp(self):
        # ~ return ide.Runner.__enter__()
    # ~ def tearDown(self):
        # ~ return ide.Runner.__exit_()


logging.info('z. IDE_utils // START/CONNECT/.run./ADAPT/STOP')
class _IDE(_connect, unittest.TestCase):
    def tearDown(self):
        geany.stop()


if __name__ == '__main__':

    from IDE_utils import stop as killall
    unittest.main()
    geany.killall_soffice
